import axios from 'axios';
import { apiBase } from '../config/constants';

export default class Request {

    static get(url, params) {
        console.log(`Getting ${url}`)

        return axios.get(url,  {
            // params: {
                // pageToken:''
            // },
            headers: {
                authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
    }

    static post(url, payload) {
        console.log(`Posting ${url}`)

        return axios({
            method: 'post',
            url: `${apiBase}${url}`,
            data: payload,
            headers: {
                authorization: `Bearer ${localStorage.getItem('token')}`
            }
        });       
    }
}