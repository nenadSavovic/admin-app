import Request from "./request.service";
import { jwt_decode } from 'jwt-decode';
import { authTokenKey } from "../config/constants";
var jwtDecode = require('jwt-decode');


export default class SessionService {

    constructor() {
        this.scheduler = null;

        const token = localStorage.getItem(authTokenKey);
        if(token) {
            this.scheduleTokenRefresh();
        }
    }

    setSession(token) {
        localStorage.setItem(authTokenKey, token);
        this.scheduleTokenRefresh();
    }

    scheduleTokenRefresh(interval) {
        const token = localStorage.getItem(authTokenKey);
        const decoded = jwtDecode(token);
        const remaining = decoded.exp - (new Date()).getTime()/1000;
        
        this.scheduler = setTimeout(this.refreshToken.bind(this), (remaining - 2*60)*1000);
    }

    refreshToken() {
        Request.post('token/refresh',{
            token: localStorage.getItem(authTokenKey)
        }).then(res => {
            this.scheduleTokenRefresh();
        }).catch(err => {
            console.error('Unable to refresh token!');
        })
    }

}