import {LOGIN} from "./types";
import {LOGOUT} from "./types";
import {ERROR} from "./types";
import {GET_DATA} from "./types";
import {GET_TABLE_DATA} from './types';
import {GET_TOKEN_DATA} from './types'
import {GET_USER_DATA} from './types'

import axios from 'axios';
import Request from "../services/request.service";
import { authTokenKey } from "../config/constants";



export const login = (res) => {
    return {
        type: LOGIN,
        payload: res
    }
}

export const getData = () => async dispatch =>{
  const res = await Request.get('https://g-api.glimpse.me/admin/dashboard/stats')
  dispatch ({type: GET_DATA, payload: res.data})
}

export const getUserListData = () => async dispatch =>{
  const res = await Request.get('https://g-api.glimpse.me/admin/user/list')
  dispatch ({type: GET_USER_DATA, payload: res.data})
}

export const getTableData = (id, pageToken) => async dispatch => {
  const res = await Request.get('https://g-api.glimpse.me/admin/table/cassandra/' + id + '?pageToken=' + pageToken)
  dispatch ({type: GET_TABLE_DATA, payload: res.data.data})
}

export const getTokenData = (id, pageToken) => async dispatch => {
  const res = await Request.get('https://g-api.glimpse.me/admin/table/token/' + id + '?pageToken=' + pageToken)
  dispatch ({type: GET_TOKEN_DATA, payload: res.data.data})
}

export const login2 = ( username, password ) => {
    return dispatch => {
        
      let config = {
        headers: {
            'Content-Type': 'application/json'
      },
      body: JSON.stringify({ username, password })
    }
      axios
        .post('https://g-api.glimpse.me/admin/auth', {
            "userName": username,
            "password": password
        }, config) 

        .then(res => {
          localStorage.setItem('token', res.data.data.jwtToken);

          dispatch(login(res));     
           // update session
          // SessionService.setSession(res.data.data.jwtToken);
        }).catch(error => {
          console.log(error)
          dispatch({type: ERROR})
        }) 
        
    };
}

export const logout = () => {
  
  localStorage.removeItem(authTokenKey);
  return {
    type: LOGOUT
  }
}