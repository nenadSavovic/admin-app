
import { LOGIN, LOGOUT, ERROR, GET_DATA , GET_TABLE_DATA, SET_KEY, GET_TOKEN_DATA, GET_USER_DATA} from "../actions/types";
import { combineReducers } from "redux";
import * as _ from 'lodash';

const token = localStorage.getItem('token') || '';

const initialStateLogin = { isLogedIn: false, token: token, error: false };

const initialStateStats = {
    totalUsers: 0,
    onlineUsers: 0 
}
const initialStateTable = {
    nextPage: '',
    pageToken: '',
    data: [],
    keyNames: []
}

const initialStateToken = {
    data:[],
    pageToken: '',
    keyNames: [],
    nextPage: ''
}

const initialStateUsers = {
    data:{}
}

export const loginReducer = (state = initialStateLogin, action) => {
    switch (action.type) {
        case LOGIN:
            return { isLogedIn: true, token: action.payload.data.data.jwtToken, error: false };
        case LOGOUT:
            return { isLogedIn: false, token: "" }
        case ERROR:
            return {...state, error: true}
        default:
            return state;
    }
};

export const dataReducer = (state=initialStateStats, action) => {
    switch (action.type) {
        case GET_DATA:
            return {totalUsers: action.payload.data.users, onlineUsers: action.payload.data.online}
        default:
            return state;
    }
}

export const userListReducer = (state=initialStateUsers, action) => {
    switch (action.type) {
        case GET_USER_DATA:
            return {...state, data:action.payload.data}
        default:
            return state;
    }
}

export const tableReducer = (state=initialStateTable, action) => {
    switch (action.type) {
        case GET_TABLE_DATA:
                let x = action.payload.data[0];
                let keyNames = []
                _.forIn(x, function (value, key) {
                    keyNames.push(key);
                });
            return {...state, data: action.payload.data, nextPage: action.payload.nextPage, keyNames: keyNames}
        case SET_KEY: 
            return {...state, keyNames: action.payload.keyNames}
        default:
            return state;
    }
}

export const tokenReducer = (state= initialStateToken, action) => {
    switch (action.type) {
        case GET_TOKEN_DATA:
                let x = action.payload.data[0];
                let keyNames = []
                _.forIn(x, function (value, key) {
                    keyNames.push(key);
                });
            return {...state, data:action.payload.data, nextPage: action.payload.nextPage, keyNames: keyNames}
        default:
            return state;
    }
}

const rootReducer = combineReducers({
    auth: loginReducer,
    dashboardData: dataReducer,
    tableData: tableReducer,
    tokenData: tokenReducer,
    userData: userListReducer
})

export default rootReducer;