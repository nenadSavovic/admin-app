import React from 'react';
import './index.css';
import HomePage from './components/Homepage'
import Login from './components/Login';
import { history } from './components/history';
import 'bootstrap/dist/css/bootstrap.css';
import { Route, Router, Switch } from 'react-router-dom';
import { PrivateRoute } from './components/PrivateRoute'

function App() {
  return (
    <div >
      <Router history={history}>
        <Switch>
          <PrivateRoute path="/home" component={HomePage} />
          <Route path="/" exact component={Login} />
          <Route path="/login" exact component={Login} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
