import React, { Component } from 'react'
import Request from '../services/request.service';
import Table from 'react-bootstrap/Table';
import { Link } from 'react-router-dom';


class TokenDB extends Component {
    state = {
        data: null
    }

    componentDidMount() {

        Request.get('https://g-api.glimpse.me/admin/table/token')
            .then(res => {
                console.log(res)

                this.setState({
                    data: res.data.data.data
                })
            })
            .catch(err => console.log(err))
    }

    render() {
        let dbData = this.state.data ? (
            this.state.data.map((user, index) =>
                <tr key={index}>
                    <td>
                    <Link to={`/home/tokentables/${user}`}>{user}</Link>
                    </td>
                    
                </tr>
            )
        ) : <tr><td colSpan="4">Loading...</td></tr>

        return (
            <div id="dbTablesWrapper" className="container">
                <h2>Token DB</h2>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>Table</th>
                        </tr>
                    </thead>
                    <tbody>
                        {dbData}
                    </tbody>
                </Table>
            </div>
        )
    }
}

export default TokenDB;
