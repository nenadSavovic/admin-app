import React, { Component } from 'react'
import Request from '../services/request.service';
import Table from 'react-bootstrap/Table';
import { Link } from 'react-router-dom';


class CasandraDB extends Component {
    state = {
        data: null
    }

    componentDidMount() {

        Request.get('https://g-api.glimpse.me/admin/table/cassandra')
            .then(res => {
                console.log(res)

                this.setState({
                    data: res.data.data
                })
            })
            .catch(err => console.log(err))
    }

    render() {
        let dbData = this.state.data ? (
            this.state.data.map((user, index) =>
                <tr key={index}>
                    <td>
                    <Link to={`/home/casandratables/${user.table_name}`}>
                    {user.table_name}</Link>
                    </td>
                    <td>{user.keyspace_name}</td>
                </tr>
            )
        ) : <tr><td colSpan="4">Loading...</td></tr>

        return (
            <div id="dbTablesWrapper" className="container">
                <h2>Cassandra DB</h2>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>Table</th>
                            <th>Keyspace</th>
                        </tr>
                    </thead>
                    <tbody>
                        {dbData}
                    </tbody>
                </Table>
            </div>
        )
    }
}

export default CasandraDB;
