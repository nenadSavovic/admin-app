import React, { Component } from 'react'
import Request from '../services/request.service';
import Table from 'react-bootstrap/Table';



class AdminUserDB extends Component {
    state = {
        data: null
    }

    componentDidMount() {

        Request.get('https://g-api.glimpse.me/admin/adminUser')
            .then(res => {
                console.log(res)

                this.setState({
                    data: res.data.data
                })
            })
            .catch(err => console.log(err))
    }

    render() {
        let dbData = this.state.data ? (
            this.state.data.map((user, index) =>
                <tr key={index}>
                    <td>{user.email}</td>
                    <td>{user.firstName}</td>
                    <td>{user.lastName}</td>
                    <td>{user.phone}</td>
                    <td>{user.roles}</td>
                    <td>{user.adminUserName}</td>
                </tr>
            )
        ) : <tr><td colSpan="6">Loading...</td></tr>

        return (
            <div id="dbTablesWrapper" className="container">
                <h2>Admin User DB</h2>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>Email</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Phone</th>
                            <th>Roles</th>
                            <th>Admin userName</th>
                        </tr>
                    </thead>
                    <tbody>
                        {dbData}
                    </tbody>
                </Table>
            </div>
        )
    }
}

export default AdminUserDB;