import React from 'react';
import { Link, Route } from 'react-router-dom';
import Header from './Header';
import Dashboard from './OnlineStats';
import CasandraDB from './CasandraDB';
import CasandraTables from './CasandraTables';
import TokenDB from './TokenDB';
import TokenTables from './TokenTables';
import AdminUserDB from './AdminUserDB';
import UsersDB from './UsersDB';

class Homepage extends React.Component {

    render() {
        return (
            <div>
                <Header />
                <ul>
                    <li><Link to="/home/dashboard">Online stats</Link></li>
                    <li><Link to="/home/casandradb">Casandra DB</Link></li>
                    <li><Link to="/home/tokendb">Token DB</Link></li>
                    <li><Link to="/home/admindb">Admin User DB</Link></li>
                    <li><Link to="/home/usersdb">Users DB</Link></li>
                </ul>
                
                <Route path="/home/dashboard" component={Dashboard}/>
                <Route path="/home/casandradb" component={CasandraDB}/>
                <Route path="/home/admindb" component={AdminUserDB}/>
                <Route path="/home/usersdb" component={UsersDB}/>
                <Route exact path="/home/casandratables/:id" component={CasandraTables}/>
                <Route path="/home/tokendb" component={TokenDB} />
                <Route exact path="/home/tokentables/:id" component={TokenTables}/>
            </div>
        )
    }
}

export default Homepage;