import React, { Component } from 'react'
import { logout } from '../actions'
import { history } from './history'
import { connect } from 'react-redux';
import admin from '../images/admin2.png'


 class Header extends Component {

    logoutAndRedirect = () => {
        this.props.logout();
        history.push('/login')
    }

    render() {
        return (
            <div id="headerWrapper">
                <h1 id="headerLogo">GlimpseAdmin <img src={admin} alt="admin icon"/> </h1>
                <button id="logoutButton" onClick={this.logoutAndRedirect} className=" btn btn-primary float-right" >Logout</button>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        logout: function () {
            dispatch(logout())
        }
    }
}

export default connect(null, mapDispatchToProps)(Header);
