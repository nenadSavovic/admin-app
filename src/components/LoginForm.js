import React from 'react';
import { login, login2 } from '../actions'
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import admin from '../images/admin2.png'

class LoginForm extends React.Component {

    state = {
        username: "",
        password: "",
        usernameError: "",
        passwordError: ""

    }

    validate = () => {
        let usernameError = "";
        let passwordError = "";


        if (!this.state.username) {
            usernameError = "username cannot be blank";
        }

        if (!this.state.password) {
            passwordError = "password cannot be blank";
        }

        if (usernameError || passwordError) {
            this.setState({ usernameError, passwordError });
            return false;
        }

        return true;
    };

    handleChange = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.value;

        this.setState({
            [name]: value
        });
    }

    handleSubmit = (event) => {

        event.preventDefault();
        this.props.loginFunction2(this.state.username, this.state.password);
        const isValid = this.validate();
        if (isValid) {
            // clear form
            //this.setState(this.state)
        }
    };

    render() {

        if (this.props.token) {
            return <Redirect to="/home" />
        }
        return (
            <div className="container">
               <h1> GlimpseAdmin <img src={admin} alt="admin icon"/> </h1>
                <br/>
                <br/>
                <h2> Login to start your session </h2>
                <br />
                {this.props.error && <h2 style={{ fontSize: 16, color: "red" }}>There was an error, try again</h2>}
                <form >
                    <div>
                        <input className="form-control"
                            id="username"
                            type="text"
                            name="username"
                            placeholder="username"
                            onChange={this.handleChange}
                        />
                        <div style={{ fontSize: 12, color: "red" }}>
                            {this.state.usernameError}
                        </div>
                    </div>

                    <br/>

                    <div>
                        <input className="form-control"

                            id="password"
                            type="password"
                            name="password"
                            placeholder="password"
                            onChange={this.handleChange}
                        />
                        <div style={{ fontSize: 12, color: "red" }}>
                            {this.state.passwordError}
                        </div>
                    </div>

                </form>
                <br />
                <button className="btn btn-primary"
                    onClick={this.handleSubmit} type="submit">Login</button>
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        login: state.auth.isLogedIn,
        token: state.auth.token,
        error: state.auth.error
    }
}

const mapDispatchToProps = dispatch => {
    return {
        loginFunction: function (username, password) {
            dispatch(login(username, password))
        },
        loginFunction2: function (username, password) {
            dispatch(login2(username, password))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);
