import React from 'react'
import Table from 'react-bootstrap/Table';
import { connect } from 'react-redux';
import { getTokenData } from '../actions';
//import { sortBy } from 'lodash'

class TokenTables extends React.Component {


    componentDidMount() {
        const { match: { params } } = this.props;
        this.props.getTokenData(params.id, this.props.tokenData.pageToken);
    }

    goToFirstPage = () => {
        console.log('tu smo');
        this.props.getTokenData(this.props.match.params.id, '');
    }

    goToNextPage = () => {
        let nextPageToken = this.props.tokenData.nextPage;
        this.props.getTokenData(this.props.match.params.id, nextPageToken)
    } 

    render() {
        if (false) {
            return <h1>aaaaaaa</h1>
        }
        const { match: { params } } = this.props;
        //console.log(this.props.tokenData);
        let dbData = this.props.tokenData ? (
            this.props.tokenData.data.map((user, index) =>

                <tr key={index}>
                    {
                        this.props.tokenData.keyNames.map((name) => {
                            let value = user[name];
                            if (value == null || value instanceof Object) {
                                if (value && value.uuid) {
                                    return <td key={name}>{value.uuid}</td>
                                }
                                return <td key={name}>{name}</td>
                            }
                            return <td key={name}>{value}</td>
                        })
                    }

                </tr>
            )
        ) : <tr><td colSpan="4">Loading...</td></tr>

        return (
            <div id="dbTablesWrapper" className="container">
                <h2>Token {params.id}</h2>
                <button onClick={this.goToFirstPage}>first page</button>
                {this.props.tokenData.nextPage && <button onClick={this.goToNextPage}>next page</button>}
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            {
                                this.props.tokenData.keyNames.map((name) => {
                                    return <th key={name}>{name}</th>
                                })
                            }

                        </tr>
                    </thead>
                    <tbody>
                        {dbData}
                    </tbody>
                </Table>
            </div>
        )
    }
}
const mapStateToProps = (state) => {
   return {
    tokenData: state.tokenData
   } 
}

export default connect(mapStateToProps, {getTokenData})(TokenTables);
