import React, { Component } from 'react'
import { connect } from 'react-redux';
import {getUserListData} from '../actions'
import Table from 'react-bootstrap/Table';


 class UsersDB extends Component {

    componentDidMount() {
        this.props.getUserListData()
    }

    render() {
        let users = this.props.userData.data.data;
        let dbData = users ? (
            users.map((user, index) =>
                <tr key={index}>
                    <td>{user.createdAt}</td>
                    <td>{user.username}</td>
                    <td>{user.gender}</td>
                    <td>{user.phone}</td>
                    <td>{user.first}</td>
                    <td>{user.wid}</td>
                    <td>{user.last}</td>
                    <td>{user.email}</td>
                    <td>{user.accountType}</td>
                </tr>
            )
        ) : <tr><td colSpan="9">Loading...</td></tr>

        return (
            <div id="dbTablesWrapper" className="container">
                <h2>User List DB</h2>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>createdAt</th>
                            <th>username</th>
                            <th>gender</th>
                            <th>Phone</th>
                            <th>first</th>
                            <th>wid</th>
                            <th>last</th>
                            <th>email</th>
                            <th>accountType</th>
                        </tr>
                    </thead>
                    <tbody>
                        {dbData}
                    </tbody>
                </Table>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        userData: state.userData
    }
}

export default connect(mapStateToProps, {getUserListData})(UsersDB);
