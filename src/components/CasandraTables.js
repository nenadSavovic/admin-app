import React from 'react'
import Table from 'react-bootstrap/Table';
import { connect } from 'react-redux';
import { getTableData } from '../actions';
//import { sortBy } from 'lodash'

class CasandraTables extends React.Component {


    componentDidMount() {
        const { match: { params } } = this.props;
        this.props.getTableData(params.id, this.props.tableData.pageToken);
    }

    goToFirstPage = () => {
        console.log('tu smo');
        this.props.getTableData(this.props.match.params.id, '');
    }

    goToNextPage = () => {
        let nextPageToken = this.props.tableData.nextPage;
        this.props.getTableData(this.props.match.params.id, nextPageToken)
    } 

    render() {
        if (false) {
            return <h1>aaaaaaa</h1>
        }
        const { match: { params } } = this.props;
        //console.log(this.props.tableData);
        let dbData = this.props.tableData ? (
            this.props.tableData.data.map((user, index) =>

                <tr key={index}>
                    {
                        this.props.tableData.keyNames.map((name) => {
                            let value = user[name];
                            if (value == null || value instanceof Object) {
                                if (value && value.uuid) {
                                    return <td key={name}>{value.uuid}</td>
                                }
                                return <td key={name}>{name}</td>
                            }
                            return <td key={name}>{value}</td>
                        })
                    }

                </tr>
            )
        ) : <tr><td colSpan="4">Loading...</td></tr>

        return (
            <div id="dbTablesWrapper" className="container">
                <h2>Cassandra {params.id}</h2>
                {this.props.tableData.nextPage === '' && <button onClick={this.goToFirstPage}>first page</button>}
                {this.props.tableData.nextPage && <button onClick={this.goToNextPage}>next page</button>}
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            {
                                this.props.tableData.keyNames.map((name) => {
                                    return <th key={name}>{name}</th>
                                })
                            }

                        </tr>
                    </thead>
                    <tbody>
                        {dbData}
                    </tbody>
                </Table>
            </div>
        )
    }
}
const mapStateToProps = (state) => {
   return {
    tableData: state.tableData
   } 
}

export default connect(mapStateToProps, {getTableData})(CasandraTables);