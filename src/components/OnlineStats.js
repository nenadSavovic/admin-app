import React from 'react'
import { connect } from 'react-redux';
import { getData } from '../actions'

class OnlineStats extends React.Component {


    componentDidMount() {
        this.props.getData();
    }

    render() {

        return (
            <div id="dashboardWrapper" >
                <h2>Online stats</h2>
                Total number of users: <b>{this.props.data.totalUsers}</b>
                <br />
                Online users: <b>{this.props.data.onlineUsers}</b>

            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        data: state.dashboardData
    }
}

export default connect(mapStateToProps, { getData })(OnlineStats);

